﻿using Blog.Data;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Blog.Data.Models
{
    public class ArticleModel : BaseEntity
    {
        [Required]
        public string Title { get; set; }
        [Required]
        public string Content { get; set; }
        
        public string Thumbnail { get; set; }

        [HiddenInput]
        [Required]
        public Int64 UserId { get; set; }

        [DataType(DataType.Date)]
        [HiddenInput]
        public DateTime DateCreate { get; set; }
    }
}
