﻿using Blog.Data;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Blog.Data.Models
{
    public class CommentModel : BaseEntity
    {
        [HiddenInput]
        public Int64 Id { get; set; }
        [HiddenInput]
        [Required]
        public Int64 ArticleID { get; set; }
        [Required]
        public string CommentDetail { get; set; }
        [Required]
        public string CommentTime { get; set; }
        [HiddenInput]
        [Required]
        public string CommentUser { get; set; }

        public ArticleModel Article { get; set; }
    }
}
