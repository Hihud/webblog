﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Blog.Data
{
    class CommentMap
    {
        public CommentMap(EntityTypeBuilder<Comment> entityTypeBuilder)
        {
            entityTypeBuilder.HasKey(t => t.Id);
            entityTypeBuilder.Property(t => t.CommentDetail);
            entityTypeBuilder.Property(t => t.CommentUser);
            entityTypeBuilder.HasOne(t => t.Article).WithMany(t => t.Comment).HasForeignKey(x => x.ArticleID);
            entityTypeBuilder.Property(t => t.CommentTime);
        }
    }
}
