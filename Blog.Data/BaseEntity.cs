﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Blog.Data
{
    public class BaseEntity
    {
        public Int64 Id { get; set; }
    }
}
