﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Blog.Data
{
    class ArticleMap
    {
        public ArticleMap(EntityTypeBuilder<Article> entityTypeBuilder)
        {
            entityTypeBuilder.HasKey(t => t.Id);
            entityTypeBuilder.Property(t => t.Title).IsRequired();
            entityTypeBuilder.Property(t => t.Content).IsRequired();
            entityTypeBuilder.Property(t => t.DateCreate);
        }
    }
}
