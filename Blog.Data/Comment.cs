﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Blog.Data
{
    public class Comment:BaseEntity
    {
       // public Int64 Id { get; set; }
        public Int64 ArticleID { get; set; }
        public string CommentDetail { get; set; }
        [DataType(DataType.Date)]
        public DateTime CommentTime { get; set; }
        public string CommentUser { get; set; }
        public Article Article { get; set; }
    }
}
