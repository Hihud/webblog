﻿using Blog.Data.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Blog.Data
{
    public class Article:BaseEntity
    {
    //    public Int64 Id { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public string UserId { get; set; }
        public string Thumbnail { get; set; }
        public DateTime DateCreate { get; set; }
        [ForeignKey("UserId")]
        public virtual ApplicationUser User { get; set; }

     
        public ICollection<Comment> Comment { get; set; }
    }
}
