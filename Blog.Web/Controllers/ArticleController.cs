﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Blog.Data;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;
using Blog.Repo;
using Blog.Data.Models;
using Blog.Service;
using Microsoft.AspNetCore.Http;
using System.IO;
using ContosoUniversity;
using ReflectionIT.Mvc.Paging;

namespace Blog.Controllers
{
    [Authorize]
    public class ArticleController : Controller
    {
        private readonly IArticleService _articleService;
        private readonly ICommentService _commentService;
        private readonly ApplicationContext _appContext;
        private readonly UserManager<ApplicationUser> _userManager;
        public ArticleController(IArticleService articleService, ICommentService commentService, UserManager<ApplicationUser> userManager, ApplicationContext appContext)
        {
            this._articleService = articleService;
            this._commentService = commentService;
            this._userManager = userManager;
            this._appContext = appContext;
        }

        public async Task<IActionResult> Index(int page = 1)
        {
            var qry = _appContext.Article.AsNoTracking().OrderByDescending(p => p.DateCreate);
            var model = await PagingList.CreateAsync(qry, 5, page);
            return View(model);
        }

        public IActionResult Upload()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Upload(ArticleModel model, IFormFile thumb)
        {
            if (ModelState.IsValid)
            {

                Article article = new Article
                {
                    Thumbnail = Path.GetExtension(thumb.FileName),
                    Title = model.Title,
                    Content = model.Content,
                    UserId = _userManager.GetUserId(User),
                    DateCreate = DateTime.Now,
                };
                _articleService.InsertArticle(article);
                var path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "images", $"{article.Id}{article.Thumbnail}");
                using (var stream = new FileStream(path, FileMode.Create))
                {
                    await thumb.CopyToAsync(stream);
                }


                return RedirectToAction(nameof(Index));
            }
            return RedirectToAction("Index", "Home");
        }

        public IActionResult ArticleDetail(Int64 id)
        {
            Article article = _articleService.GetArticle(id);

            if (article == null)
            {
                return NotFound();
            }
            ViewBag.UserId = _userManager.GetUserId(User);
            var a = article.UserId;
            return View(article);

        }
        public IActionResult Edit(Int64 id)
        {
            Article article = _articleService.GetArticle(id);
            if (article == null)
            {
                return NotFound();
            }

            return View(article);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit([Bind("ArticleID,Title,Content")] Article model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    Article article = new Article
                    {
                        Id = model.Id,
                        Title = model.Title,
                        Content = model.Content,
                    };
                    _articleService.UpdateArticle(article);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ArticleModelExists(model.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(model);
        }

        public IActionResult Delete(Int64 id)
        {
            _articleService.DeleteArticle(_articleService.GetArticle(id));
            return View("Index", _articleService.GetArticle());
        }
        private bool ArticleModelExists(Int64 id)
        {
            return _articleService.GetArticle(id) != null;
        }


    }
}