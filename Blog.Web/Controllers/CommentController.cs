﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Blog.Service;
using Blog.Data;
using Blog.Data.Models;
using Microsoft.AspNetCore.Identity;

namespace Blog.Web.Controllers
{
    public class CommentController : Controller
    {
        private readonly IArticleService _articleService;
        private readonly ICommentService _commentService;
        private readonly UserManager<ApplicationUser> _userManager;

        public CommentController(IArticleService articleService, ICommentService commentService, UserManager<ApplicationUser> userManager)
        {
            this._articleService = articleService;
            this._commentService = commentService;
            this._userManager = userManager;
        }
        public IActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public IActionResult _AddComment(Int64 Id,string contentComment)
        {
            
            Comment comment = new Comment
            {
                ArticleID = Id,
                CommentDetail = contentComment,
                CommentTime = DateTime.Now,
                CommentUser = _userManager.GetUserName(User)
            };
            Article article = _articleService.GetArticle(Id);
            if (article.Comment == null)
            {
                article.Comment = new List<Comment>();
                article.Comment.Add(new Comment
                {
                    ArticleID = Id,
                    CommentDetail = contentComment,
                    CommentTime = DateTime.Now,
                    CommentUser = _userManager.GetUserName(User)
                });
            }
            else
            {
                article.Comment.Add(comment);
            }
                    
            _commentService.InsertComment(comment);
            _articleService.UpdateArticle(article);
            return RedirectToAction("ArticleDetail","Article",new {id= article.Id });
        }

       



    }
}