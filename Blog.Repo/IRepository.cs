﻿using System;
using System.Collections.Generic;
using System.Text;
using Blog.Data;
using Microsoft.EntityFrameworkCore;

namespace Blog.Repo
{
    public interface IRepository<T> where T : BaseEntity
    {
        DbSet<T> Entity { get; set; }

        IEnumerable<T> GetAll();
        T Get(long id);
        void Insert(T entity);
        void Update(T entity);
        void Delete(T entity);
        void Remove(T entity);
        void SaveChanges();
    }
}
