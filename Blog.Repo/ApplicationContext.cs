﻿using System;
using System.Collections.Generic;
using System.Text;
using Blog.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Blog.Data.Models;

namespace Blog.Repo
{
    public class ApplicationContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationContext(DbContextOptions<ApplicationContext> options) : base(options)
        {
        }
        public DbSet<Article> Article { get; set; }
        public DbSet<Comment> Comment { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            //new UserMap(modelBuilder.Entity<User>());
            //new UserProfileMap(modelBuilder.Entity<UserProfile>());
        }
    }

}
