﻿using Blog.Data;
using Blog.Repo;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace Blog.Service
{
    public class ArticleService:IArticleService
    {
        private IRepository<Article> _articleRepository;

        public ArticleService(IRepository<Article> articleRepository)
        {
            this._articleRepository = articleRepository;
        }

        public IEnumerable<Article> GetArticle()
        {
            return _articleRepository.GetAll();
        }

        public Article GetArticle(long id)
        {
            return _articleRepository.Entity.Include(m => m.Comment).First(m => m.Id == id);
        }

        public List<Article> GetRecentArticle()
        {
            List<Article> articleRecent = _articleRepository.GetAll().OrderByDescending(a => a.DateCreate).Take(3).ToList();
            return articleRecent;

        }

        public void InsertArticle(Article article)
        {
            _articleRepository.Insert(article);
        }
        public void UpdateArticle(Article article)
        {
            _articleRepository.Update(article);
        }
        public void DeleteArticle(Article article)
        { 
            _articleRepository.Delete(article);
        }
    }
}
