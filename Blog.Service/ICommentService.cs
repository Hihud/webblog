﻿using Blog.Data;
using System;
using System.Collections.Generic;
using System.Text;

namespace Blog.Service
{
    public interface ICommentService
    {
        IEnumerable<Comment> GetComment(long idArticle);
        void InsertComment(Comment comment);
        List<Comment> GetRecentComment();
    }
}
