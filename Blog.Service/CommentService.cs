﻿using Blog.Data;
using Blog.Repo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Blog.Service
{
    public class CommentService: ICommentService
    {
        private readonly IRepository<Comment> _commentRepository;

        public CommentService(IRepository<Comment> commentRepository)
        {
            this._commentRepository = commentRepository;
        }
        public IEnumerable<Comment> GetComment(long idArticle)
        {
            return _commentRepository.GetAll().Where(m => m.ArticleID == idArticle).OrderByDescending(d => d.CommentTime).Take(10).ToList();
        }

        public List<Comment> GetRecentComment()
        {
            return _commentRepository.GetAll().OrderByDescending(d => d.CommentTime).Take(5).ToList();
        }

        public void InsertComment(Comment comment)
        {
            _commentRepository.Insert(comment);
        }

    }
}
