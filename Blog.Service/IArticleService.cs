﻿using System;
using System.Collections.Generic;
using Blog.Data;
using System.Text;

namespace Blog.Service
{
    public interface IArticleService
    {
        IEnumerable<Article> GetArticle();
        Article GetArticle(long id);
        void InsertArticle(Article article);
        void UpdateArticle(Article article);
        void DeleteArticle(Article article);
        List<Article> GetRecentArticle();

    }
}
